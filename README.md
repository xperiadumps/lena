qssi-user 13 62.2.A.0.481 058000A003003102854466984 release-keys

manufacturer: Sony

platform: lito

codename: XQ-BT52

flavor: qssi-user

release: 13

id: 62.2.A.0.481

incremental: 062002A000048103716344522

tags: release-keys

fingerprint: Sony/qssi/qssi:13/62.2.A.0.481/062002A000048103716344522:user/release-keys

brand: Sony

branch: qssi-user-13-62.2.A.0.481-062002A000048103716344522-release-keys

repo: lena
